import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { useHook } from './context'
import { ActionType } from './reducer'
export const config: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json' },
    withCredentials: true,
    timeout: 60000
}
const service = axios.create(config)
service.defaults.headers.common['Content-Type'] = 'application/json'
/*
const requestHandler = (request: any) => {
    console.log('requestHandler', request)
    const { dispatch } = useHook
    request.headers['Content-Type'] = 'application/json'
    return request
}
service.interceptors.request.use(
    (request: any) => requestHandler(request)
)
*/
export const errorHandler = (error: AxiosError): unknown => {
    const { dispatch } = useHook
    let msg = error.toString()
    if (error.response && error.response.data) {
        msg = error.response.data.message
    }
    dispatch({ type: ActionType.SET_ERRORMSG, data: { errorMsg: msg } })
    if (msg === 'Cancel: axios request cancelled') return
    return Promise.reject({ ...error })
}
export const successHandler = (response: AxiosResponse): AxiosResponse => {
    const { dispatch } = useHook
    if (response && response.headers && response.headers['csrf']) {
        service.defaults.headers.common['Authorization'] = `csrf ${response.headers['csrf']}`
        dispatch({ type: ActionType.SET_CSRF, data: { csrf: response.headers['csrf'], ip: response.headers['x-ip'] } })
    }
    dispatch({ type: ActionType.SET_ERRORMSG, data: { errorMsg: undefined } })
    return response
}
service.interceptors.response.use(successHandler, errorHandler)

export default service