import { reducres, ActionType } from './'

describe('test reducer', () => {
    it('test reducer', (done: () => void) => {
        expect(reducres({}, { type: undefined, data: {} })).toEqual({})
        done()
    })
    it('test SET_CSRF', (done: () => void) => {
        expect(reducres({ csrf: '' }, { type: ActionType.SET_CSRF, data: { csrf: '12as' } })).toEqual({ csrf: '12as' })
        done()
    })
    it('test SET_ERRORMSG', (done: () => void) => {
        expect(reducres({ errorMsg: '' }, { type: ActionType.SET_ERRORMSG, data: { errorMsg: 'error' } })).toEqual({ errorMsg: 'error' })
        done()
    })
    it('test CLEAR', (done: () => void) => {
        expect(reducres({}, { type: ActionType.CLEAR, data: {} })).toEqual({})
        done()
    })
})