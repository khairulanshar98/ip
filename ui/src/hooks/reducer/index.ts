import { data, initialData } from '../store'

export enum ActionType {
    SET_CSRF = 'SET_CSRF',
    SET_ERRORMSG = 'SET_ERRORMSG',
    CLEAR = 'CLEAR',
}

export interface IAction {
    type: ActionType | undefined
    data: data
}

export const reducres = (state: data, action: IAction): data => {
    Object.freeze(state)
    const data: data = { ...state }
    switch (action.type) {
        case ActionType.SET_CSRF:
            data.csrf = action.data.csrf
            data.ip = action.data.ip
            return data
        case ActionType.SET_ERRORMSG:
            data.errorMsg = action.data.errorMsg
            return data
        case ActionType.CLEAR:
            return initialData
        default:
            return data
    }
}

