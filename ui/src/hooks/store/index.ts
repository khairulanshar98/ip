export interface data {
    csrf?: string | undefined
    ip?: string | undefined
    errorMsg?: string | undefined
}

export const initialData: data = {
    csrf: undefined,
    ip: undefined,
    errorMsg: undefined
}