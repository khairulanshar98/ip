import React, { ReactElement } from 'react'
import { data, initialData } from '../store'
import { IAction, reducres } from '../reducer'

interface axiosHook {
    dispatch: React.Dispatch<IAction>,
    setDispatch: (dispacth: React.Dispatch<IAction>) => void
}

export const dispatcher = (): void => undefined

export const useHook: axiosHook = {
    dispatch: (): void => undefined,
    setDispatch: function (dispatch: React.Dispatch<IAction>) {
        this.dispatch = dispatch
    },
}

const AppContext = React.createContext<[data, React.Dispatch<IAction>]>([initialData, dispatcher])
export const useContext = (): [data, React.Dispatch<IAction>] => React.useContext(AppContext)

const Provider: React.FC = (props): ReactElement => {
    const [store, dispatch] = React.useReducer(reducres, initialData)
    React.useEffect(() => {
        useHook.setDispatch(dispatch)
    }, [])
    /*
    React.useEffect(() => {
        console.log(store)
    }, [store])
    */
    return (
        <AppContext.Provider value={[store, dispatch]}>
            {props.children}
        </AppContext.Provider>
    )
}

export default Provider