import service from '../hooks/AxiosHooks'

export const getData = (page = 1, limit = 5, filter = '@'): Promise<unknown> => {
    if (filter === '') filter = '@'
    return service.get(`/api/v1/request/${page}/${limit}/${encodeURIComponent(filter)}`)
}
