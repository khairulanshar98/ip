import service from '../hooks/AxiosHooks'

export const checkToken = (): Promise<unknown> => {
    return service.get('/api/v1/checktoken')
}
