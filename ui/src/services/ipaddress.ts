import service from '../hooks/AxiosHooks'

export const getData = (page = 1, limit = 5, filter = '@'): Promise<unknown> => {
    if (filter === '') filter = '@'
    return service.get(`/api/v1/ip/${page}/${limit}/${encodeURIComponent(filter)}`)
}

export const patchData = (data: unknown, page = 1, limit = 5, filter = '@'): Promise<unknown> => {
    if (filter === '') filter = '@'
    return service.patch(`/api/v1/ip/${page}/${limit}/${encodeURIComponent(filter)}`, data)
}
