import React, { ReactElement } from 'react'
import Paper from '@material-ui/core/Paper'
import { fade, makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import Switch from '@material-ui/core/Switch'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { useContext } from '../../hooks/context'
import { getData, patchData } from '../../services/ipaddress'

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: theme.spacing(4),
        paddingTop: theme.spacing(4),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        width: '100%',
        boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
        backgroundColor: theme.palette.secondary.light,
    },
    container: {
        marginTop: theme.spacing(2),
    },
    button: {
        marginBottom: theme.spacing(1),
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.40),
        '&:hover': {
            backgroundColor: theme.palette.common.white,
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '50%',
            float: 'left'
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '60ch',
        },
    },
    searchCheckbox : {
        paddingLeft: theme.spacing(2),
    }
}))

export interface IpAddressDocument {
    _id: string
    ipAddress: string
    count: number
    createdAt: Date
    updatedAt: Date
    enable: boolean
}

export const dateFormat = (d: Date): string => `${new Date(d).toLocaleDateString()} ${new Date(d).toLocaleTimeString()}`


const List: React.FC = (): ReactElement => {
    const classes = useStyles()
    const [store] = useContext()
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const [serchText, setSerchText] = React.useState('')
    const [rows, setRows] = React.useState<IpAddressDocument[]>([])
    const [count, setCount] = React.useState<number>(0)
    const [mypIp, setMyIp] = React.useState(false)
    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage)
    }
    const loadData = (res: any) => {
        if (res.data?.data) {
            setRows(res.data?.data)
            setCount(res.data?.count)
        }
    }
    const searchData = (ip: string): void => {
        getData(page + 1, rowsPerPage, ip).then(loadData)
    }
    const patch = (data: IpAddressDocument): void => {
        patchData(data, page + 1, rowsPerPage, serchText).then(loadData)
    }
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(+event.target.value)
        setPage(0)
    }
    const search = (): void => {
        searchData(serchText)
    }
    const clear = (): void => {
        setSerchText('')
        searchData('')
    }
    React.useEffect(() => {
        if (store.csrf) searchData(serchText)
    }, [store.csrf, page, rowsPerPage])
    React.useEffect(() => {
        if (store.csrf)
            if (mypIp && store.ip) {
                setSerchText(store.ip)
                searchData(store.ip)
            } else {
                setSerchText('')
                searchData('')
            }
    }, [mypIp])

    return (
        <Container>
            <Paper className={classes.root}>
                <div className={classes.container}>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder='Search IP Address'
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                            value={serchText}
                            onChange={e => setSerchText(e.target.value)}
                            disabled={mypIp}
                        />
                    </div>
                    <Button title='search' variant='contained' onClick={search} disabled={mypIp}>Search</Button>
                    <Button title='reset' variant="contained" color="secondary" onClick={clear} style={{ marginLeft: '8px' }} disabled={mypIp}> Clear</Button>
                </div>
                <div className={classes.container}>
                    <div className={classes.searchCheckbox}>
                        <FormControlLabel
                            control={<Checkbox
                                checked={mypIp}
                                data-testid='mypIp'
                                onChange={() => setMyIp(p => !p)}
                                inputProps={{ 'aria-label': 'IP Address checkbox' }}
                            />}
                            label="Show my IP address only"
                        />
                    </div>
                </div>
                <TableContainer className={classes.container}>
                    <Table stickyHeader aria-label='sticky table'>
                        <TableHead>
                            <TableRow>
                                <TableCell style={{ width: '30%' }}>
                                    IP Address
                                </TableCell>
                                <TableCell style={{ width: '10%' }}>
                                    Count
                                </TableCell>
                                <TableCell style={{ width: '20%' }}>
                                    Created At
                                </TableCell>
                                <TableCell style={{ width: '20%' }}>
                                    Updated At
                                </TableCell>
                                <TableCell style={{ width: '20%' }}>
                                    Status
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.slice().map((row, i) =>
                                <TableRow hover role='checkbox' tabIndex={-1} key={i}>
                                    <TableCell style={{ width: '30%' }}>
                                        {row.ipAddress}
                                    </TableCell>
                                    <TableCell style={{ width: '10%' }}>
                                        {row.count}
                                    </TableCell>
                                    <TableCell style={{ width: '20%' }}>
                                        {dateFormat(row.createdAt)}
                                    </TableCell>
                                    <TableCell style={{ width: '20%' }}>
                                        {dateFormat(row.updatedAt)}
                                    </TableCell>
                                    <TableCell style={{ width: '20%' }}>
                                        <Switch
                                            data-testid={`switch-${i}`}
                                            checked={row.enable}
                                            onChange={
                                                (event: React.ChangeEvent<HTMLInputElement>) => {
                                                    patch({
                                                        ...row,
                                                        ...{ enable: event.target.checked }
                                                    })
                                                }}
                                            name={row.ipAddress}
                                            inputProps={{ 'aria-label': row.ipAddress }}
                                        />
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25, 100]}
                    component='div'
                    count={count}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </Container>
    )
}

export default List