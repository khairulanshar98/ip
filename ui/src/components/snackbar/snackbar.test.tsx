import '@testing-library/jest-dom'
import React from 'react'
import { render, screen, fireEvent, act } from '@testing-library/react'
import Provider, { useContext } from '../../hooks/context/index'
import { ActionType } from '../../hooks/reducer'
import ErrorSnackbar from './'

const Comp = () => {
    const [store, dispatch] = useContext()
    React.useEffect(() => {
        dispatch({ type: ActionType.SET_ERRORMSG, data: { errorMsg: '123av' } })
    }, [])

    const click = () => {
        dispatch({ type: ActionType.SET_ERRORMSG, data: { errorMsg: undefined } })
    }

    return (<>
        <div data-testid='test-id' onClick={click}>
            {store.errorMsg}
        </div>
        <ErrorSnackbar />
    </>)
}

describe('test context', () => {
    it('Provider', async (done: () => void) => {
        await act(async () => {
            render(<Provider><Comp /></Provider>)
        })
        await act(async () => {
            const winner = await screen.getByTestId('test-id')
            expect(winner).toBeInTheDocument()
            expect(winner).toHaveTextContent(/123av/i)
            fireEvent.click(winner)
        })
        await act(async () => {
            const Close = await screen.getByTitle('Close')
            fireEvent.click(Close)
        })
        done()
    })
})