import React, { ReactElement } from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'
import { useContext } from '../../hooks/context'
import { ActionType } from '../../hooks/reducer'

const ErrorSnackbar: React.FC = (): ReactElement => {
    const [store, dispatch] = useContext()
    const [open, setOpen] = React.useState(false)
    const handleClose = () => {
        dispatch({ type: ActionType.SET_ERRORMSG, data: { errorMsg: undefined } })
        setOpen(false)
    }
    React.useEffect(() => {
        setOpen(!!store.errorMsg)
    }, [store.errorMsg])

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
            open={open}
            title='Snackbar'
            onClose={handleClose}>
            <MuiAlert
                onClose={handleClose}
                elevation={6}
                variant="filled"
                severity="error">
                {store.errorMsg}
            </MuiAlert>
        </Snackbar>
    )
}

export default ErrorSnackbar