import React, { ReactElement } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link'
import { Link as RouterLink, RouteComponentProps } from 'react-router-dom'
import clsx from 'clsx'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginBottom: theme.spacing(8),
    },
    title: {
      color: theme.palette.common.white,
      paddingRight: theme.spacing(2),
    },
    menu: {
      color: theme.palette.common.white,
      paddingRight: theme.spacing(2),
    },
    selected: {
      color: theme.palette.info.main,
      fontWeight: 600
    },
  }),
)

const AppBarComponent: React.FC<RouteComponentProps> = (props: RouteComponentProps): ReactElement => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography data-testid='title-app-bar' className={classes.title} variant="h6" noWrap>
            Khairul Anshar
          </Typography>
          <Link
            title='ip-app-bar'
            variant="body1"
            underline="none"
            className={clsx(classes.menu, props.location.pathname === '/' ? classes.selected : undefined)}
            component={RouterLink}
            to="/"
          >
            IP Address
          </Link>
          <Link
            title='client-app-bar'
            variant="body1"
            underline="none"
            className={clsx(classes.menu, props.location.pathname === '/client' ? classes.selected : undefined)}
            component={RouterLink}
            to="/client"
          >
            Clien Request
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default AppBarComponent