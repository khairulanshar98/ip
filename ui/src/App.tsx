import React, { ReactElement, Suspense } from 'react'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import CssBaseline from '@material-ui/core/CssBaseline'
import AppBarComponent from './components/header'
import { checkToken } from './services/session'
import ErrorSnackbar from './components/snackbar'
import { Route, Switch, RouteComponentProps } from 'react-router-dom'
import Loading from './components/loading'
const IpAddress = React.lazy(() => import('./pages/IpAddress'))
const Client = React.lazy(() => import('./pages/Client'))

const useStyles = makeStyles(() =>
  createStyles({
    '@global': {
      body: {
        backgroundColor: '#f5f5f5',
        overflowX: 'hidden'
      },
      ul: {
        margin: 0,
        padding: 0,
        listStyle: 'none',
      },
    },
    root: {
      overflowX: 'hidden'
    },
  }),
)

const App: React.FC<RouteComponentProps> = (props: RouteComponentProps): ReactElement => {
  const classes = useStyles()
  React.useEffect(() => {
    checkToken().catch(() => undefined)
  }, [])
  return (
    <Box className={classes.root}>
      <CssBaseline />
      <AppBarComponent {...props} />
      <ErrorSnackbar />
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route exact path="/" component={IpAddress} />
          <Route path="/client" component={Client} />
        </Switch>
      </Suspense>
    </Box>
  )
}

export default App
