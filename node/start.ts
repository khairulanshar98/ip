import env from 'dotenv'
import configEnv from './src/common/config'
env.config()
import app from './src/server'
import startDB from './src/startDb'
const init = async () => {
    await startDB(configEnv.DB_STR)
    const server = app.listen(process.env.PORT || 80, async () => {
        console.info('App listening ', server.address())
    })
    server.timeout = parseInt(configEnv.DEFAULT_TIMEOUT)
}
init()