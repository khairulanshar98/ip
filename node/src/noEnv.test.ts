import env from 'dotenv'
env.config()
import supertest from 'supertest'
import app from './server'
import startDB, { stopDB } from './startDb'

it('checktoken with db', async (done) => {
    await startDB(process.env.DB_STR || '')
    const request = supertest(app)
    const res: any = await request.get('/api/v1/checktoken')
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')
    await stopDB()
    done()
})
