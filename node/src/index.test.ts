import supertest from 'supertest'
import app from './server'
const request = supertest(app)
jest.setTimeout(60000)
it('checktoken no db', async (done) => {
    const res: any = await request.get('/api/v1/checktoken')
    expect(res.status).toBe(404)
    expect(res.body.status ).toBe('ERROR')
    done()
})