import env from 'dotenv'
env.config()
import supertest from 'supertest'
import app from '../server'
import startDB, { stopDB } from '../startDb'
import IpAddressModel from '../repository/IpAddress'

it('checkBruteForce', async (done) => {
    await startDB(process.env.DB_STR || '')
    const request = supertest(app)
    const now = new Date().getTime()
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 9, epoch: now })
    let res: any = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')

    res = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', Accept: 'application/json' })
    expect(res.status).toBe(429)
    expect(res.body.status).toBe('ERROR')

    res = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', Accept: 'application/json' })
    expect(res.status).toBe(429)
    expect(res.body.status).toBe('ERROR')

    const minutes = Number(process.env.TRY_IN_MINUTES || 1) + 10
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, epoch: now - (minutes * 60 * 1000) })
    res = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')

    await IpAddressModel.upsert({ ipAddress: 'foo', enable: false })
    res = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', Accept: 'application/json' })
    expect(res.status).toBe(403)
    expect(res.body.status).toBe('ERROR')
    await stopDB()
    done()
})
