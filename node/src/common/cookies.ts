import jws from 'jws'
import jsonwebtoken from 'jsonwebtoken'
import { Request, Response } from 'express'
import configEnv from './config'
import { getIp } from './checkBruteForce'

export const getCookies = (cookie: string): string => {
    const cookies = cookie.split(';')
    for (const cookie of cookies) {
        const kv = cookie.split('=')
        if (kv[0].trim() === configEnv.COOKIE)
            return kv[1].trim()
    }
    return ''
}

export const getAuthorization = (authorization: string): string | undefined => {
    const data = authorization.split(' ')
    if (data.length == 1) return data[0]
    return data[1]
}

export const decodeToken = (jwt: string, secret:string): jws.Signature | undefined => {
    try {
        if (jwt && jsonwebtoken.verify(jwt, secret)) {
            return jws.decode(jwt)
        }
    } catch (e) {
        console.log(e)
    }
    return undefined
}

export const createToken = (data: any): string => {
    return jsonwebtoken.sign(data, configEnv.SECRET, { expiresIn: configEnv.TOKEN_MAXAGE })
}

export const cookieObject = (): any => {
    return {
        secure: /true/i.test(configEnv.TOKEN_SECURE),
        httpOnly: /true/i.test(configEnv.TOKEN_HTTPONLY),
        path: '/',
        domain: configEnv.COOKIE_DOMAIN,
        maxAge: configEnv.TOKEN_MAXAGE,
        SameSite: 'None'
    }
}

export const getToken = (req: Request): jws.Signature | undefined => {
    return decodeToken(getCookies(req.headers['cookie'] || ''), configEnv.SECRET)
}

export const isCsrfValid = (req: Request, res: Response, next: () => void): any => {
    const token = getToken(req)
    if (token) {
        const obj = token.payload
        const csrf = getAuthorization(req.headers.authorization || '')
        const ip = getIp(req)
        if (obj.csrf === csrf && obj.ip === ip) {
            delete obj.exp
            delete obj.iat
            const newToken = createToken(obj)
            res.cookie(configEnv.COOKIE, newToken, cookieObject())
            return next()
        }
    }
    return res.status(403).send({
        status: 'ERROR',
        message: 'insufficient permission.'
    })
}