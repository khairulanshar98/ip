import env from 'dotenv'
env.config()

interface config {
    COOKIE: string
    COOKIE_DOMAIN: string
    SECRET: string
    TOKEN_MAXAGE: string
    NODE_ENV: string
    TOKEN_SECURE: string
    TOKEN_HTTPONLY: string
    DB_STR: string
    MAX_REQUEST: number
    TRY_IN_MINUTES: number
    PORT: string
    DEFAULT_TIMEOUT: string
    FILE_LIMIT: string
}
const configEnv: config = {
    COOKIE: `${process.env.COOKIE}`,
    COOKIE_DOMAIN: `${process.env.COOKIE_DOMAIN}`,
    SECRET: `${process.env.SECRET}`,
    TOKEN_MAXAGE: `${process.env.TOKEN_MAXAGE}`,
    NODE_ENV: `${process.env.NODE_ENV}`,
    TOKEN_SECURE: `${process.env.TOKEN_SECURE}`,
    TOKEN_HTTPONLY: `${process.env.TOKEN_HTTPONLY}`,
    DB_STR: `${process.env.DB_STR}`,
    MAX_REQUEST: Number(`${process.env.MAX_REQUEST}`),
    TRY_IN_MINUTES: Number(`${process.env.TRY_IN_MINUTES}`),
    PORT: `${process.env.PORT}`,
    DEFAULT_TIMEOUT: `${process.env.DEFAULT_TIMEOUT}`,
    FILE_LIMIT: `${process.env.DEFAULT_TIMEOUT}`,
}

export default configEnv