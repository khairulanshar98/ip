import configEnv from './config'
import { createToken, decodeToken, getAuthorization, getCookies } from './cookies'

it('x-real-ip', async (done) => {
    expect(getCookies('ip=cookie; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly'))
        .toBe('cookie')
    expect(getAuthorization('csrf'))
        .toBe('csrf')
    expect(getAuthorization('first csrf'))
        .toBe('csrf')

    const token: string = createToken({ data: 'csrf' })
    const result: any = decodeToken(token, configEnv.SECRET)
    expect(result.payload.data)
        .toBe('csrf')
    done()
})
it('invalid signature', async (done) => {
    const token: string = createToken({ data: 'csrf' })
    const result: any = decodeToken(token, 'a')
    expect(result).toBeUndefined()
    done()
})
