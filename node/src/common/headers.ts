import { Request, Response } from 'express'
import configEnv from './config'

export const noCache = (req: Request, res: Response, next: () => void): void => {
    res.setHeader('Cache-Control', 'private, no-cache, must-revalidate')
    res.setHeader('Expires', '-1')
    next()
}

export const setHeaders = (req: Request, res: Response, next: () => void): void => {
    res.setHeader('Server', '')
    res.setHeader('X-Powered-By', '')
    res.setHeader('X-Content-Type-Options', 'nosniff')
    res.setHeader('X-XSS-Protection', '1; mode=block')
    res.setHeader('Access-Control-Allow-Origin', configEnv.COOKIE_DOMAIN)
    res.setHeader('X-Frame-Options', 'SAMEORIGIN')
    res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains')
    res.setHeader('Content-Security-Policy', `default-src 'none';connect-src 'self';script-src 'self' 'unsafe-inline';style-src 'self' 'unsafe-inline';img-src 'self';media-src 'self';font-src 'self';manifest-src 'self';frame-src 'self';`)
    next()
}
