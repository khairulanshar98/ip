import { Request, Response } from 'express'
import IpAddressModel, { IpAddress } from '../repository/IpAddress'
import ClientModel, { Client } from '../repository/Client'
import configEnv from './config'

export const getIp = (req: Request): string => {
    const proxy_ip = req.headers['x-real-ip'] || req.headers['x-forwarded-for']
    if (proxy_ip) return `${proxy_ip}`
    return req.ip
}

const checkBruteForce = async (req: Request, res: Response, next: () => void): Promise<any> => {
    try {
        const IpAddress: IpAddress = { ipAddress: getIp(req) }
        let ipAddressDocument: any = await IpAddressModel.upsert(IpAddress)
        if (!ipAddressDocument.enable) {
            return res.status(403.6).send({
                status: 'ERROR',
                errorCode: 403.6,
                message: `IP address ${IpAddress.ipAddress} rejected`
            })
        }
        const now: number = new Date().getTime()
        const delta: number = now - ipAddressDocument.epoch
        const max = configEnv.MAX_REQUEST
        const minutes = configEnv.TRY_IN_MINUTES
        if (ipAddressDocument.count < max) {
            ipAddressDocument = await IpAddressModel.increaseCount(IpAddress)
            const Client: Client = { ipAddress: ipAddressDocument._id, path: req.originalUrl }
            await ClientModel.increaseCount(Client)
            return next()
        } else if (delta >= minutes * 60 * 1000) {
            ipAddressDocument.epoch = now
            ipAddressDocument.count = 0
            await ipAddressDocument.save()
            return next()
        } else {
            if (ipAddressDocument.count === max) {
                ipAddressDocument.epoch = now
                await ipAddressDocument.save()
            }
            ipAddressDocument = ipAddressDocument.toObject()
            res.set('Retry-After', String(ipAddressDocument.epoch))
            return res.status(429).send({
                status: 'ERROR',
                errorCode: 429,
                message: `Too Many Requests, Try again in next ${minutes} minute`
            })
        }
    } catch (e) {
        console.log('error')
    }
    return res.status(404).send({
        status: 'ERROR',
        errorCode: 404,
        message: 'Not found'
    })
}

export default checkBruteForce