import express from 'express'
import compression from 'compression'
import path from 'path'
import cookieParser from 'cookie-parser'
const app = express()
import { setHeaders, noCache } from './common/headers'
import checkBruteForce from './common/checkBruteForce'
import routes from './routes'
import configEnv from './common/config'

app.use(compression())
app.use(express.json({
    limit: configEnv.FILE_LIMIT
}))
app.use(express.urlencoded({
    limit: configEnv.FILE_LIMIT
}))
app.use(cookieParser())
app.use(setHeaders, express.static(path.join(__dirname, '../build')))
app.use('/api', checkBruteForce, noCache, setHeaders, routes)
app.use('/*', setHeaders, express.static(path.join(__dirname, '../build')))

export default app