import express, { Request, Response } from 'express'
import IpAddressModel from '../../repository/IpAddress'

const router = express.Router()

router.get('/:page/:limit/:filter', async (req: Request, res: Response) => {
  const data = await IpAddressModel.search(Number(req.params.page), Number(req.params.limit), req.params.filter.replace('@', ''))
  const count = await IpAddressModel.countDocument(req.params.filter.replace('@', ''))
  return res.json({ status: 'OK', data, count })
})

router.patch('/:page/:limit/:filter', async (req: Request, res: Response) => {
  try {
    let data = await IpAddressModel.findById(req.body._id)
    if (data) {
      data.enable = req.body.enable
      await data.save()
    }
    data = await IpAddressModel.search(Number(req.params.page), Number(req.params.limit), req.params.filter.replace('@', ''))
    const count = await IpAddressModel.countDocument(req.params.filter.replace('@', ''))
    return res.json({ status: 'OK', data, count })
  } catch (e) {
    return res.json({ status: 'NOT OK' })
  }  
})

export default router