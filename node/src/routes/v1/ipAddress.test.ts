import env from 'dotenv'
env.config()
import supertest from 'supertest'
import { createToken } from '../../common/cookies'
import app from '../../server'
import startDB, { stopDB } from '../../startDb'
import IpAddressModel from '../../repository/IpAddress'

it('ipAddress', async (done) => {
    await startDB(process.env.DB_STR || '')
    const now = new Date().getTime()
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })

    const request = supertest(app)

    const token: string = createToken({ csrf: '1234' })
    let res: any = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', 'cookie': `cartrack=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()

    res = await request.get('/api/v1/ip/1/5/@').set({ 'x-real-ip': 'foo', 'Authorization': `csrf ${res.headers['csrf']}`, 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')

    res = await request.get('/api/v1/ip/1/5/@').set({ 'x-real-ip': 'foo', 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
    expect(res.status).toBe(403)
    expect(res.body.status).toBe('ERROR')

    res = await request.get('/api/v1/ip/1/5/@').set({ 'x-real-ip': 'foo', 'Authorization': `csrf 123`, Accept: 'application/json' })
    expect(res.status).toBe(403)
    expect(res.body.status).toBe('ERROR')

    await stopDB()
    done()
})


it('patch', async (done) => {
    await startDB(process.env.DB_STR || '')
    const now = new Date().getTime()
    let ip: any = await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })
    const request = supertest(app)
    ip = ip.toObject()
    const token: string = createToken({ csrf: '1234' })
    let res: any = await request.get('/api/v1/checktoken')
        .set({ 'x-real-ip': 'foo', 'cookie': `cartrack=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()

    res = await request.patch('/api/v1/ip/asdfasdf/asdfasdfwe/tgyhrre')
        .set({ 'x-real-ip': 'foo', 'Authorization': `csrf ${res.headers['csrf']}`, 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
        .send(ip)
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')

    await stopDB()
    done()
})

it('patch', async (done) => {
    await startDB(process.env.DB_STR || '')
    const now = new Date().getTime()
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })
    const request = supertest(app)

    const token: string = createToken({ csrf: '1234' })
    let res: any = await request.get('/api/v1/checktoken')
        .set({ 'x-real-ip': 'foo', 'cookie': `cartrack=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()

    res = await request.patch('/api/v1/ip/asdfasdf/asdfasdfwe/tgyhrre')
        .set({ 'x-real-ip': 'foo', 'Authorization': `csrf ${res.headers['csrf']}`, 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
        .send({ _id: 's' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('NOT OK')

    await stopDB()
    done()
})

it('patch', async (done) => {
    await startDB(process.env.DB_STR || '')
    const now = new Date().getTime()
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })
    const request = supertest(app)

    const token: string = createToken({ csrf: '1234' })
    let res: any = await request.get('/api/v1/checktoken')
        .set({ 'x-real-ip': 'foo', 'cookie': `cartrack=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()

    res = await request.patch('/api/v1/ip/asdfasdf/asdfasdfwe/tgyhrre')
        .set({ 'x-real-ip': 'foo', 'Authorization': `csrf ${res.headers['csrf']}`, 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
        .send({ _id: '6060b581257fa2891cb4a946' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')

    await stopDB()
    done()
})