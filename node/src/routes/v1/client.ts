import express, { Request, Response } from 'express'
import ClientModel from '../../repository/Client'

const router = express.Router()

router.get('/:page/:limit/:filter', async (req: Request, res: Response) => {
  const data = await ClientModel.search(Number(req.params.page), Number(req.params.limit), req.params.filter.replace('@', ''))
  const count = await ClientModel.countDocument(req.params.filter.replace('@', ''))
  return res.json({ status: 'OK', data, count })
})

export default router