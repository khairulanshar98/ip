import express from 'express'
const router = express.Router()
import {isCsrfValid} from '../../common/cookies'
import checktoken from './checkToken'
import ipAddress from './ipAddress'
import client from './client'

//Non protected api
router.use('/checktoken', checktoken)

//protected api
router.use('/ip', isCsrfValid, ipAddress)
router.use('/request', isCsrfValid, client)
export default router