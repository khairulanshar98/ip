import express, { Request, Response } from 'express'
import { getToken, cookieObject, createToken } from '../../common/cookies'
import { v4 as uuidv4 } from 'uuid'
import configEnv from '../../common/config'
import { getIp } from '../../common/checkBruteForce'
const router = express.Router()

router.get('/', async (req: Request, res: Response) => {
    const token = getToken(req)
    const csrf = uuidv4().replace(/-/g, '')
    let cookie: any = {}
    if (token) {
        const obj: any = token.payload
        delete obj.exp
        delete obj.iat
        cookie = { ...obj }
    }
    cookie.csrf = csrf
    cookie.ip = getIp(req)
    res.setHeader('csrf', cookie.csrf)
    res.setHeader('x-ip', cookie.ip)
    cookie = createToken(cookie)
    res.cookie(configEnv.COOKIE, cookie, cookieObject())
    return res.json({ status: 'OK' })
})

export default router