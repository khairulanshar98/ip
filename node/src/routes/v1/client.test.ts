import env from 'dotenv'
env.config()
import supertest from 'supertest'
import { createToken } from '../../common/cookies'
import app from '../../server'
import startDB, { stopDB } from '../../startDb'
import IpAddressModel from '../../repository/IpAddress'

it('request', async (done) => {
    await startDB(process.env.DB_STR || '')
    const now = new Date().getTime()
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })

    const request = supertest(app)

    const token: string = createToken({ csrf: '1234' })
    let res: any = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', 'cookie': `cartrack=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()

    res = await request.get('/api/v1/request/1/5/@').set({ 'x-real-ip': 'foo', 'Authorization': `csrf ${res.headers['csrf']}`, 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')

    res = await request.get('/api/v1/request/1/5/@').set({ 'x-real-ip': 'foo', 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
    expect(res.status).toBe(403)
    expect(res.body.status).toBe('ERROR')

    res = await request.get('/api/v1/request/1/5/@').set({ 'x-real-ip': 'foo', 'Authorization': `csrf 123`, Accept: 'application/json' })
    expect(res.status).toBe(403)
    expect(res.body.status).toBe('ERROR')

    await stopDB()
    done()
})

it('request', async (done) => {
    await startDB(process.env.DB_STR || '')
    const now = new Date().getTime()
    await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })

    const request = supertest(app)

    const token: string = createToken({ csrf: '1234' })
    let res: any = await request.get('/api/v1/checktoken').set({ 'x-real-ip': 'foo', 'cookie': `cartrack=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()

    res = await request.get('/api/v1/request/asdfa/asdf/asdf').set({ 'x-real-ip': 'foo', 'Authorization': `csrf ${res.headers['csrf']}`, 'cookie': res.headers['set-cookie'][0], Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.body.status).toBe('OK')


    await stopDB()
    done()
})



