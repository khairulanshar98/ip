import env from 'dotenv'
env.config()
import supertest from 'supertest'
import { createToken } from '../../common/cookies'
import app from '../../server'
import startDB, { stopDB } from '../../startDb'

it('checktoken with db', async (done) => {
    await startDB(process.env.DB_STR || '')
    const request = supertest(app)

    const token: string = createToken({ csrf: '1234' })
    const res: any = await request.get('/api/v1/checktoken').set({ 'cookie': `ip=${token}; Max-Age=900; Domain=localhost; Path=/; Expires=Invalid Date; HttpOnly`, Accept: 'application/json' })
    expect(res.status).toBe(200)
    expect(res.headers['csrf']).toBeDefined()
    await stopDB()
    done()
})
