import { Document, Model, model, Types, Schema } from 'mongoose'

export interface Client {
    ipAddress: Types.ObjectId | Record<string, unknown>
    path?: string
    count?: number
    epoch?: number
}
interface ClientDocument extends Client, Document {
}
export interface ClientModel extends Model<ClientDocument> {
    increaseCount(data: Client): Promise<ClientDocument>
    search(page: number, limit: number, ip: string): Promise<ClientDocument>
    countDocument(ip: string): Promise<ClientDocument>
}

const ClientSchema = new Schema<ClientDocument, ClientModel>({
    ipAddress: { type: Schema.Types.ObjectId, ref: 'IpAddress', required: true },
    path: { type: String, default: '', required: true, trim: true },
    epoch: { type: Number, default: 0, required: true },
    count: { type: Number, default: 0, required: true },
}, { timestamps: true })
const upsertConfig = { returnNewDocument: true, upsert: true, new: true, passRawResult: true }
ClientSchema.statics.increaseCount = function (
    this: Model<ClientDocument>,
    data: Client
) {
    return new Promise((resolve) => {
        this.findOneAndUpdate(
            { ipAddress: data.ipAddress, path: data.path },
            { $inc: { count: 1 } },
            upsertConfig,
            (err, data) => {
                if (err) return resolve(null)
                resolve(data)
            }
        )
    })
}
const p = [{
    path: 'ipAddress',
    model: 'IpAddress',
    select: 'ipAddress enable'
}]
ClientSchema.statics.search = function (
    this: Model<ClientDocument>,
    page: number,
    limit: number,
    ip: string
) {
    const filter: any = {}
    const sort = { createdAt: 1 }
    if (ip.length) filter.path = new RegExp(ip, 'i')
    return new Promise((resolve) => {
        this.find(filter)
            .select('-__v')
            .populate(p)
            .sort(sort)
            .skip((page - 1) * limit)
            .limit(limit)
            .exec((err, doc) => {
                if (err) return resolve([])
                resolve(doc)
            })
    })
}
ClientSchema.statics.countDocument = function (
    this: Model<ClientDocument>,
    ip: string
) {
    const filter: any = {}
    if (ip.length) filter.path = new RegExp(ip, 'i')
    return new Promise((resolve) => {
        this.countDocuments(filter)
            .exec((err, doc) => {
                if (err) return resolve(null)
                resolve(doc)
            })
    })
}
ClientSchema.index({ path: 1, csrf: 1 })
export default model<ClientDocument, ClientModel>('ClientSchema', ClientSchema)