import { Document, Model, model, Schema } from 'mongoose'

export interface IpAddress {
    ipAddress: string | undefined
    enable?: boolean
    count?: number
    epoch?: number
}
interface IpAddressDocument extends IpAddress, Document {
}
export interface IpAddressModel extends Model<IpAddressDocument> {
    upsert(data: IpAddress): Promise<IpAddressDocument>
    increaseCount(data: IpAddress): Promise<IpAddressDocument>
    search(page: number, limit: number, ip: string): Promise<IpAddressDocument>
    countDocument(ip: string): Promise<IpAddressDocument>
}
const IpAddress = new Schema<IpAddressDocument, IpAddressModel>({
    ipAddress: { type: String, default: '', trim: true, required: true },
    enable: { type: Boolean, default: true, required: true },
    count: { type: Number, default: 0, required: true },
    epoch: { type: Number, default: 0, required: true },
}, { timestamps: true })
const upsertConfig = { returnNewDocument: true, upsert: true, new: true, passRawResult: true }
IpAddress.statics.upsert = function (
    this: Model<IpAddressDocument>,
    data: IpAddress
) {
    return new Promise((resolve) => {
        this.findOneAndUpdate(
            { 'ipAddress': data.ipAddress },
            { $set: data },
            upsertConfig,
            (err, data) => {
                if (err) return resolve(null)
                resolve(data)
            }
        )
    })
}
IpAddress.statics.increaseCount = function (
    this: Model<IpAddressDocument>,
    data: IpAddress
) {
    return new Promise((resolve) => {
        this.findOneAndUpdate(
            { ipAddress: data.ipAddress },
            { $inc: { count: 1 } },
            upsertConfig,
            (err, data) => {
                if (err) return resolve(null)
                resolve(data)
            }
        )
    })
}
IpAddress.statics.search = function (
    this: Model<IpAddressDocument>,
    page: number,
    limit: number,
    ip: string
) {
    const filter: any = {}
    const sort = { createdAt: 1 }
    if (ip.length) filter.ipAddress = new RegExp(ip, 'i')
    return new Promise((resolve) => {
        this.find(filter)
            .select('-__v')
            .sort(sort)
            .skip((page - 1) * limit)
            .limit(limit)
            .exec((err, doc) => {
                if (err) return resolve([])
                resolve(doc)
            })
    })
}
IpAddress.statics.countDocument = function (
    this: Model<IpAddressDocument>,
    ip: string
) {
    const filter: any = {}
    if (ip.length) filter.ipAddress = new RegExp(ip, 'i')
    return new Promise((resolve) => {
        this.countDocuments(filter)
            .exec((err, doc) => {
                if (err) return resolve(null)
                resolve(doc)
            })
    })
}
IpAddress.index({ ipAddress: 1 })
IpAddress.index({ enable: 1 })
export default model<IpAddressDocument, IpAddressModel>('IpAddress', IpAddress)