import mongoose from 'mongoose'
import env from 'dotenv'
env.config()
import ClientModel from './Client'
jest.setTimeout(60000)

it('ipAddress', async (done) => {
    try {
        await ClientModel.increaseCount({ ipAddress: new mongoose.Types.ObjectId('6060b580257fa2891cb4a925'), path: 'a' })
    } catch (e) {
        console.log('error')
    }
    try {
        await ClientModel.search(10000, 1, '')
    } catch (e) {
        console.log('error')
    }
    try {
        await ClientModel.countDocument('foo')
    } catch (e) {
        console.log('error')
    }

    done()
})

