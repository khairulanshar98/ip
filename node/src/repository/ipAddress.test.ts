import env from 'dotenv'
env.config()
import IpAddressModel from './IpAddress'
jest.setTimeout(60000)

it('ipAddress', async (done) => {
    const now = new Date().getTime()
    try {
        await IpAddressModel.upsert({ ipAddress: 'foo', enable: true, count: 0, epoch: now })
    } catch (e) {
        console.log('error')
    }
    try {
        await IpAddressModel.increaseCount({ ipAddress: 'foo', enable: true, count: 0, epoch: now })
    } catch (e) {
        console.log('error')
    }
    try {
        await IpAddressModel.search(10000, 1, '')
    } catch (e) {
        console.log('error')
    }
    try {
        await IpAddressModel.countDocument('foo')
    } catch (e) {
        console.log('error')
    }

    done()
})

