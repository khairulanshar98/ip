import mongoose from 'mongoose'
mongoose.Promise = global.Promise
const config: any = {
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true
}
const startDB = (DB_STR: string): any => mongoose.connect(DB_STR, config)
export const stopDB = (): any => mongoose.connection.close()

export default startDB