# README #
## Please use different terminal/ cmd to run NodeJs and ReactJs

### To start NodeJs locally ###
### `Please create .env file inside the node folder and follow these steps` ###
```
PORT=9009
DEFAULT_TIMEOUT=60000
FILE_LIMIT=1MB
COOKIE_DOMAIN=localhost
COOKIE=ip
#YOU CAN PUT ANY STRING IN SECRET TO GENERATE JWT
SECRET=asdf4355sdfg@dsger@dfwe^lasadfoiuwkfaf*asdfpal
TOKEN_MAXAGE=900000
NODE_ENV=development
TOKEN_SECURE=false
TOKEN_HTTPONLY=true
#MONGODB URI CONNECTION
DB_STR=
MAX_REQUEST=10
TRY_IN_MINUTES=1
```


### `Please follow these steps to start` ###
```
> cd node
> yarn install or npm install
> yarn test or npm test
> yarn start or npm start
```

### To start ReactJs locally ###
### `Please follow these steps` ###
```
> cd ui
> yarn install or npm install
> yarn test or npm test
> yarn start or npm start
```
